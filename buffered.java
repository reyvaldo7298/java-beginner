import java.io.*;

public class buffered{
    public static void main(String[] args) {
        BufferedReader buff = new BufferedReader (new InputStreamReader (System.in));

        int data;

        System.out.println("Masukan Angka : ");
        try {
            data = Integer.parseInt(buff.readLine());
        } catch (Exception e) {
            System.out.println("Invalid input!");
        }
    }
}