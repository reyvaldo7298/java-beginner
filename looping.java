public class looping{
    public static void main(String[] args) {
        int x = 1;
        int y = 1;

        System.out.println("Loop With For");
        System.out.println("---------------------------");
        for (int i = 1; i <= 5; i++) {
            System.out.println(i+". Hello world!");
        }

        System.out.println();
        System.out.println("Loop With Do ... While");
        System.out.println("---------------------------");
        do {
            System.out.println(x+". Hello Surakarta!");
            x++;
        } while (x <= 5);

        System.out.println();
        System.out.println("Loop With While");
        System.out.println("---------------------------");
        while (y <= 5) {
            System.out.println(y+". Hello Solo!");
            y++;
        }
    }
}