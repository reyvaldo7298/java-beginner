public class array{
    public static void main(String[] args) {
        String singleString[] = {"one","two","three","four","five"};
        int singleInt[] = {1,2,3,4,5};
        
        String multiString[][] = {{"one","satu"},{"two","dua"},{"three","tiga"},{"four","empat"},{"five","lima"}};
        int multiInt[][] = {{1,2},{3,4},{5,6},{7,8},{9,0}};
        
        System.out.println("Array 1 Dimensi");
        System.out.println("---------------------------");
        for (int i = 0; i < singleString.length; i++) {
            System.out.println("Array singleString["+i+"] = "+singleString[i]);
        }

        System.out.println();
        System.out.println("Array Multidimensi");
        System.out.println("---------------------------");
        for (int i = 0; i < multiString.length; i++) {
            for (int j = 0; j < 2; j++) {
                System.out.println("Array multiString["+i+"]["+j+"] = "+multiString[i][j]);
            }
            System.out.println();
        }
    }
}